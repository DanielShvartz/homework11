#include "threads.h"

int main()
{
	callWritePrimesMultipleThreads(1, 1000, "output.txt", 3);
	getchar();
	callWritePrimesMultipleThreads(1, 100000, "output.txt", 4);
	getchar();
	callWritePrimesMultipleThreads(1, 1000000, "output.txt", 100);
	system("pause");
	return 0;
}