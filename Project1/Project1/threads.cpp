#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread my_thread(I_Love_Threads);
	my_thread.join();
}

/*
The function gets a range and add all the number that are prime in the range to a vector
*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true; // we start that the number is prime
	for (int prime = begin; prime <= end; prime++) // we check all the numbers in the range
	{
		isPrime = true;
		for (int j = 2; j <= prime / 2; j++) // run on all dividers
			if (prime%j == 0) // if a number has a divider its not a prime
			{
				isPrime = false; // we say that he is not prime
				break; // stop checking
			}
		if(isPrime) // if he is prime its true
			primes.push_back(prime); // and we push him to the primes
	}
}
void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
		cout << primes[i] << endl;
}
vector<int> callGetPrimes(int begin, int end)
{
	clock_t start_time = clock();

	vector<int> primeV;
	thread t(getPrimes, begin, end, ref(primeV));
	t.join();

	clock_t end_time = clock();
	cout << "The action took: " << 1000 * ((double)end_time - start_time) / CLOCKS_PER_SEC << " m/s" << endl;
		
	return primeV;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	bool isPrime = true;
	for (int prime = begin; prime <= end; prime++) // run on the range
	{
		isPrime = true; // start that he is prime
		for (int i = 2; i <= prime / 2; i++) // run on his divides
			if (prime%i == 0) // if it a divider
			{
				isPrime = false; 
				break; // we stop the inner loop
			}
		if (isPrime) // if he is prime he doesnt have dividers
			file << prime << endl; // write to file.
	}
	file << "Finish!" << endl;
}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	thread* arr = new thread[N]; // we create an array of threads
	ofstream file;
	file.open("output.txt");

	clock_t start_time = clock(); // we start to calculate the time

	for (int i = 0; i < N; i++) // and then we run on n threads, n segmants
	{
		arr[i] = thread(writePrimesToFile, ((end - begin)*(i)) / N, ((end - begin)*(i + 1)) / N, ref(file));
		arr[i].join();
	}

	clock_t end_time = clock();
	file.close();
	cout << "The action took: " << 1000 * ((double)end_time - start_time) / CLOCKS_PER_SEC << " m/s" << endl;
}